﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CameraMovement : NetworkBehaviour
{
    public GameObject player;
    public GameObject mainCamera;

    Vector3 cameraPos;

    // Use this for initialization
    void Start()
    {
        cameraPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (isLocalPlayer)
        {
            mainCamera.SetActive(false);

            cameraPos.x = player.transform.position.x;
            cameraPos.y = player.transform.position.y;

            transform.position = cameraPos;
        }
        else
            mainCamera.SetActive(true);
    }
}
