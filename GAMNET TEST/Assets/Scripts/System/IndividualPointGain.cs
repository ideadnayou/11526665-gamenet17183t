﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class IndividualPointGain : NetworkBehaviour
{
    public int ScorePerKill = 10;

    private PlayerData data;

    private void Start()
    {
        data = GetComponent<PlayerData>();
    }
    [ClientRpc]
    public void RpcAddToScore()
    {
        if (isLocalPlayer)
        {

            data.currentScore += ScorePerKill;
            data.killCount++;
        }
    }

}
