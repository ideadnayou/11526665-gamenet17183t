﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
public class Timer : NetworkBehaviour {
    public float TotalTimeInSeconds;
    [SyncVar] public float currentTime = 0f;
    [SyncVar] public float minutes;
    [SyncVar] public float tenthsSeconds;
    [SyncVar] public float onesSeconds;
    [SyncVar] public bool mainTimer;
    public Text TimerUI;
    [SyncVar] public bool stopTime;
    [SyncVar] public bool pauseTime;
    [SyncVar] public bool startGame;
    [SyncVar] public bool levelEnd;
    //public NetworkManager networkManager;
    [SyncVar] private int totalPlayer;
    Timer serverTimer;
    private RoundSystem roundHandler;


    void Awake()
    {
        roundHandler = GetComponent<RoundSystem>();
        totalPlayer = NetworkManager.singleton.numPlayers;
        currentTime = TotalTimeInSeconds;
        currentTime -= Time.deltaTime;
        minutes = Mathf.Floor(currentTime / 60f);
        tenthsSeconds = Mathf.Floor(currentTime % 60.0f / 10f);
        onesSeconds = Mathf.Floor(currentTime % 60.0f % 10f);
        Clamp();
        stopTime = true;
        if (!startGame)
            TimerUI.gameObject.SetActive(false);
    }

    private void Start()
    {
        TimerUI.gameObject.SetActive(true);
        CheckPlayers();


    }
    void Update()
    {
        // if (mainTimer)
        if (!startGame)
            CheckPlayers();
        if (!stopTime && startGame && isServer && isClient)
            {
                currentTime -= Time.deltaTime;
                minutes = Mathf.Floor(currentTime / 60f);
                tenthsSeconds = Mathf.Floor(currentTime % 60.0f / 10f);
                onesSeconds = Mathf.Floor(currentTime % 60.0f % 10f);
                Clamp();
            if (EndTimerCondition())
            {
                GoNextRoundState();
            }

            if (startGame && !stopTime)
                TimerUI.text = minutes.ToString() + " : " + tenthsSeconds.ToString() + " " + onesSeconds.ToString();
        }
        else if (isClient && !isServer)
        {
            if (startGame && !stopTime)
                TimerUI.text = minutes.ToString() + " : " + tenthsSeconds.ToString() + " " + onesSeconds.ToString();
            else if (startGame && stopTime)
            {
                levelEnd = true;
                TimerUI.text = "TEMPORARY: GAME END";
            }
        }


        if (startGame && NetworkManager.singleton.numPlayers <= 1 && isServer && isClient)
            NetworkManager.singleton.StopHost();

    }
    void GoNextRoundState()
    {
        if (!roundHandler.IsFinalRoundAlready())
        {
            roundHandler.CurrentStage++;
            roundHandler.SetState();
            currentTime = roundHandler.CurrentStateDuration;
            currentTime -= Time.deltaTime;
            minutes = Mathf.Floor(currentTime / 60f);
            tenthsSeconds = Mathf.Floor(currentTime % 60.0f / 10f);
            onesSeconds = Mathf.Floor(currentTime % 60.0f % 10f);
        }
        else if (roundHandler.IsFinalRoundAlready())
        {

            stopTime = true;
            levelEnd = true;
            TimerUI.text = "TEMPORARY: GAME END";
        }
    }
    void CheckPlayers()
    {
        totalPlayer = NetworkManager.singleton.numPlayers;
        
        Debug.Log(totalPlayer);
        if (isServer && isClient)
        {

            if (totalPlayer > 1)
            {

                roundHandler.SetState();
                currentTime = roundHandler.CurrentStateDuration;
                startGame = true;
                stopTime = false;
                if (startGame)
                    TimerUI.text = minutes.ToString() + " : " + tenthsSeconds.ToString() + " " + onesSeconds.ToString();
            }
            else if (totalPlayer <= 1)
            {
                TimerUI.text = "Waiting for other players";
            }
        }
    }
    void Clamp()
    {
        if (currentTime <= 0)
            currentTime = 0;
        if (minutes <= 0)
            minutes = 0;
        if (tenthsSeconds <= 0)
            tenthsSeconds = 0;
        if (onesSeconds <= 0)
            onesSeconds = 0;
    }

    public bool EndTimerCondition()
    {
        if (currentTime <= 0 && minutes <= 0 && tenthsSeconds <= 0 && onesSeconds <= 0)
            return true;
        else
            return false;
    }
    public void SetStopTime(bool stopTimeCondition)
    {
        stopTime = stopTimeCondition;
    }
    public bool IsTimeEnd()
    {
        return stopTime;
    }
    public void SetCurrentTime(float currentTimeValue)
    {
        currentTime = currentTimeValue;
    }

    public float GetMinutes()
    {
        return minutes;
    }

    public float GetTenthsSeconds()
    {
        return tenthsSeconds;
    }

    public float GetOnesSeconds()
    {
        return onesSeconds;
    }

}
