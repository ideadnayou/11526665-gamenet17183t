﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
public class Results : NetworkBehaviour {
    private PlayerData data;
    public GameObject ResultCanvas;
    public Text ResultText;
    private GameObject winner;
    private Timer time;
    private void Start()
    {
        data = GetComponent<PlayerData>();
        time = GameObject.FindGameObjectWithTag("SystemHandler").GetComponent<Timer>();
        ResultCanvas.SetActive(false);
    }
    public void ShowResult()
    {
        if (isLocalPlayer)
        {
            ResultCanvas.SetActive(true);
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            int currentHighestKillCount = 0;
            foreach (GameObject player in players)
            {
                PlayerData data = player.GetComponent<PlayerData>();
                if (data.killCount >= currentHighestKillCount)
                {
                    currentHighestKillCount = data.killCount;
                    winner = player;
                }
            }
            if (winner == gameObject)
                ResultText.text = "YOU WON";
            else
                ResultText.text = "YOU LOSE";
        }

    }
    public void Update()
    {
        if (isLocalPlayer)
        {
            if (time.levelEnd)
            {
                ShowResult();
            }
        }
    }
    public void Shutdown()
    {
        NetworkManager.singleton.StopHost();
    }
}
