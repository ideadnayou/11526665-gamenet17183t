﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
public class UpgradeScreen : NetworkBehaviour
{
    //use data here
    public PlayerData data;

    //add ui here
    public GameObject MainUpgradeCanvas;
    public Text PointText;
    public Text Stat1Text;
    public Text Stat2Text;
    public Text Stat3Text;
    public int UpgradeCost = 10;

    private void Start()
    {
        data = GetComponent<PlayerData>();

        if (!isLocalPlayer)
        {
            MainUpgradeCanvas.SetActive(false);

        }
        PlaceDataOnText();
    }
    private void Update()
    {
        PlaceDataOnText();
        HardCodedOpenClose();
    }

    void PlaceDataOnText()
    {
        PointText.text = data.currentScore.ToString();
        Stat1Text.text = data.maxHP.ToString();
        Stat2Text.text = data.maxSpd.ToString();
        Stat3Text.text = data.dmgReduce.ToString();
    }
    [ClientRpc]
    public void RpcUpgradeButton(int test)
    {
        if (isLocalPlayer)
        {
            float score = data.currentScore;
            //HARD CODED
            if (test == 0 && score > 0)
            {
                float stat = data.maxHP;
                stat += 1;
                data.maxHP += 100;
            }

            if (test == 1 && score > 0)
            {
                float stat = data.maxSpd;
                stat += 1;
                data.maxSpd += 2;
            }

            if (test == 2 && score > 0)
            {
                float stat = data.dmgReduce;
                stat += 1;
                data.dmgReduce += 5;
            }

            score -= UpgradeCost;
            if (score <= 0)
                score = 0;
            data.currentScore = score;
        }
    }
    public void Close()
    {
        if (isLocalPlayer)
        {
            MainUpgradeCanvas.SetActive(false);
        }
    }
    public void HardCodedOpenClose()
    {
        if (isLocalPlayer)
        {
            RoundSystem system = GameObject.FindGameObjectWithTag("SystemHandler").GetComponent<RoundSystem>();
            if (system.CurrentStage == 2 || system.CurrentStage == 4)
                MainUpgradeCanvas.SetActive(true);
            else if (system.CurrentStage == 3 || system.CurrentStage == 5)
                MainUpgradeCanvas.SetActive(false);
        }
    }
}
