﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class RoundSystem : NetworkBehaviour {
    public enum RoundState
    {
        WaitingOtherPlayer,
        Round1,
        UpgradeRound1,
        Round2,
        UpgradeRound2,
        FinalRound,
    }
    //this is supposed to be hidden, but currently exposed for easy debugging.
    public float CurrentStateDuration;

    public RoundState CurrentState;
    //set the durations of each round state here
    [SyncVar] public float Round1Duration = 120f;
    [SyncVar] public float UpgradeRound1Duration = 60f;
    [SyncVar] public float Round2Duration = 120f;
    [SyncVar] public float UpgradeRound2Duration = 60f;
    [SyncVar] public float FinalRoundDuration = 120f;
    [SyncVar] public int CurrentStage = 0;
    private Timer timeHandler;
    private void Start()
    {
        CurrentState = RoundState.WaitingOtherPlayer;
    }
    public void CheckState()
    {
        switch (CurrentState)
        {
            case RoundState.WaitingOtherPlayer:
                CurrentStateDuration = 0f;
                break;
            case RoundState.Round1:
                CurrentStateDuration = Round1Duration;
                break;
            case RoundState.UpgradeRound1:
                CurrentStateDuration = UpgradeRound1Duration;
                break;
            case RoundState.Round2:
                CurrentStateDuration = Round2Duration;
                break;
            case RoundState.UpgradeRound2:
                CurrentStateDuration = UpgradeRound2Duration;
                break;
            case RoundState.FinalRound:
                CurrentStateDuration = FinalRoundDuration;
                break;

        }
    }
    public void SetState()
    {
        if (CurrentStage == 0)
            CurrentState = RoundState.WaitingOtherPlayer;
        else if (CurrentStage == 1)
            CurrentState = RoundState.Round1;
        else if (CurrentStage == 2)
        {
            CurrentState = RoundState.UpgradeRound1;
            //OpenUpgradeScreenForAll();
        }
        else if (CurrentStage == 3)
        {

            CurrentState = RoundState.Round2;
            //CloseUpgradeScreenForAll();
        }
        else if (CurrentStage == 4)
        {
            CurrentState = RoundState.UpgradeRound2;
           // OpenUpgradeScreenForAll();
        }

        else if (CurrentStage == 5)
        {

            CurrentState = RoundState.FinalRound;
            //CloseUpgradeScreenForAll();
        }

        CheckState();
    }
    public bool IsFinalRoundAlready()
    {
        if (CurrentStage == 5)
            return true;
        else
            return false;
    }
    public void SetState(RoundState state)
    {
        CurrentState = state;
    }
    void OpenUpgradeScreenForAll()
    {
        if(isServer && isClient || !isServer && isClient)
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject player in players)
            {
                player.GetComponent<UpgradeScreen>().MainUpgradeCanvas.SetActive(true);
            }
        }

    }
    void CloseUpgradeScreenForAll()
    {
        if (isServer && isClient || !isServer && isClient)
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject player in players)
            {
                player.GetComponent<UpgradeScreen>().MainUpgradeCanvas.SetActive(false);
            }
        }
    }
}
