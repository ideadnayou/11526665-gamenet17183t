﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class PlayerMovement : NetworkBehaviour
{
    
    public float AccelerationFactor = 10;
    public bool IsMoving;

    float maxSpeed;
    float minSpeed;
    public float speed;

    PlayerData player;

    // Use this for initialization
    void Start()
    {
        player = GetComponent<PlayerData>();

        maxSpeed = player.maxSpd;
        minSpeed = player.minSpd;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isLocalPlayer)
            return;
        if (speed < minSpeed) speed = minSpeed;
        else if (speed > maxSpeed) speed = maxSpeed;

        transform.Translate(Vector2.up * speed * Time.deltaTime);
    }

    public void Accelerate()
    {
        speed += AccelerationFactor * Time.deltaTime;
    }

    public void Decelerate()
    {
        speed -= AccelerationFactor * Time.deltaTime;
    }

    public void RotateShip(Vector3 direction)
    {
        transform.eulerAngles += direction;
    }
}
