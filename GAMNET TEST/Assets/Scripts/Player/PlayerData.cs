﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerData : NetworkBehaviour
{
    #region ShipData
    [Header("Ship Properties")]
    public ShipType type;
    public float maxHP;
    public float fireRate;
    public float dmgReduce;
    public float maxSpd;
    public float minSpd;
    public Sprite shipSprite;

    public GameObject bullet;

    public List<Sprite> killStreak;
    #endregion

    #region GameData
    ShipData hData;
    Scene scene;
    #endregion

    #region PlayerData
    [Header("Player Properties")]
    [SyncVar]
    public float currentHP;

    [SyncVar]
    float healthFill;

    [SyncVar]
    bool _isDead = false;

    public bool isDead
    {
        get { return _isDead; }
        set { _isDead = value; }
    }

    NetworkStartPosition[] spawnPoints;

    public Image healthBar;

    public GameObject gameHud;

    public float respawnTime = 3;

    [SyncVar]
    public float currentScore = 0;

    [SyncVar]
    public int killCount = 0;

    [Header("Monobehaviours")]
    [SerializeField]
    Behaviour[] disableOnDeath;
    bool[] wasEnabled;
    #endregion

    #region DeathUI
    public GameObject deathCanvas;
    public Text countDownText;
    public float countDown;
    #endregion

    void Awake()
    {
        hData = GameObject.FindObjectOfType<GameData>().mData;
        scene = SceneManager.GetSceneByName("Main");
        SetData(hData);
        currentHP = maxHP;
    }

    void Start()
    {
        if (isLocalPlayer)
        {
            spawnPoints = FindObjectsOfType<NetworkStartPosition>();
            gameHud.SetActive(true);
        }

        //deathCanvas.SetActive(false);
        countDown = respawnTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (isDead)
        {
            countDown -= Time.deltaTime;
            StartCoroutine(Respawn());
        }

        UpdateHealth();
    }

    public void SetData(ShipData mData)
    {
        type = mData.Type;
        maxHP = mData.MaxHP;
        fireRate = mData.FireRate;
        dmgReduce = mData.DamageReduction;
        maxSpd = mData.maxSpeed;
        minSpd = mData.minSpeed;
        shipSprite = mData.sprite;

        bullet = mData.BulletPrefab;

        killStreak = mData.KillStreakSprite;

        GetComponent<SpriteRenderer>().sprite = shipSprite;
    }

    [ClientRpc]
    public void RpcTakeDamage(float amount)
    {
        if (isDead) return;

        float damage = (amount * dmgReduce)/100;
        amount -= damage;

        currentHP -= amount;

        if (currentHP <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        isDead = true;

        gameHud.SetActive(false);
        //deathCanvas.SetActive(true);

        //for (int i = 0; i < disableOnDeath.Length; i++)
        //{
        //    disableOnDeath[i].enabled = false;
        //}
    }

    public void Setup()
    {
        //wasEnabled = new bool[disableOnDeath.Length];
        //for (int i = 0; i < wasEnabled.Length; i++)
        //{
        //    wasEnabled[i] = disableOnDeath[i].enabled;
        //}

        SetDefaults();
    }

    public void SetDefaults()
    {
        isDead = false;

        currentHP = maxHP;

        countDown = respawnTime;

        gameHud.SetActive(true);
        //deathCanvas.SetActive(false);

        //for (int i = 0; i < disableOnDeath.Length; i++)
        //{
        //    disableOnDeath[i].enabled = wasEnabled[i];
        //}

    }

    IEnumerator Respawn()
    {
        yield return new WaitForSeconds(respawnTime);

        SetDefaults();

        Vector2 spawnLocation = spawnPoints[Random.Range(0, spawnPoints.Length)].transform.position;
        transform.position = spawnLocation;
    }

    void UpdateHealth()
    {
        healthFill = currentHP / maxHP;
        healthBar.fillAmount = healthFill;
    }
}