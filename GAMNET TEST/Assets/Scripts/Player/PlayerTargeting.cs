﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

 public class PlayerTargeting : NetworkBehaviour
{
    public float RotSpeed = 10;

    public Vector2 Direction;

    public Transform indicator;

    public Transform spawnLocation;

    public Camera playerCamera;

    float angle;
    
    // Update is called once per frame
    void Update()
    {
        if (!isLocalPlayer) return;

        Direction = playerCamera.ScreenToWorldPoint(Input.mousePosition) - this.transform.position;

        angle = Mathf.Atan2(Direction.y, Direction.x) * Mathf.Rad2Deg;
        Quaternion rot = Quaternion.AngleAxis(angle, Vector3.forward);
        indicator.rotation = Quaternion.Slerp(indicator.transform.rotation, rot, RotSpeed * Time.deltaTime);

    }
   
    public SpriteRenderer GetTargetSprite()
    {
        SpriteRenderer target = indicator.GetComponent<SpriteRenderer>();
        return target;
    }
}
