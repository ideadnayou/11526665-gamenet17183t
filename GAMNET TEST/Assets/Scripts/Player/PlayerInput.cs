﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class PlayerInput : NetworkBehaviour
{
    public float BulletSpawnExtension = 2f;

    public NetworkBehaviour ability;

    PlayerMovement move;

    PlayerTargeting aim;

    GameObject player;

    // Use this for initialization
    void Start()
    {
        move = GetComponent<PlayerMovement>();
        aim = GetComponent<PlayerTargeting>();
        player = this.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isLocalPlayer) return;


        if (Input.GetKey(KeyCode.W))
        {
            move.Accelerate();
            move.IsMoving = true;
        }

        if (Input.GetKey(KeyCode.A))
        {
            move.RotateShip(Vector3.forward);
        }

        if (Input.GetKey(KeyCode.D))
        {
            move.RotateShip(Vector3.back);
        }

        if (Input.GetKeyUp(KeyCode.W)) move.IsMoving = false;

        if (Input.GetMouseButtonDown(0))
        {
            Vector2 dir = (aim.playerCamera.ScreenToWorldPoint(Input.mousePosition) - this.transform.position).normalized;
            CmdFire(dir);
        }

        if (!move.IsMoving) move.Decelerate();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            ability.enabled = true;
        }

    }

    public void SetAbility(NetworkBehaviour mono)
    {

    }

    [Command]
    void CmdFire(Vector2 dir)
    {
        GameObject projectile = Instantiate(player.GetComponent<PlayerData>().bullet, aim.spawnLocation.transform.position, Quaternion.identity);
        projectile.GetComponent<Rigidbody2D>().velocity = dir * projectile.GetComponent<BaseBullet>().speed;

        NetworkServer.Spawn(projectile);
    }
}