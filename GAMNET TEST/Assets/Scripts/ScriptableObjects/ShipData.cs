﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ShipType
{
    Juggernaut,
    Scout,
    Sentinel
}

[CreateAssetMenu(menuName = "Ship/New Ship")]
public class ShipData : ScriptableObject
{
    public ShipType Type;
    public float MaxHP;
    public float FireRate;
    public float DamageReduction;
    public float maxSpeed;
    public float minSpeed;
    public Sprite sprite;

    public GameObject BulletPrefab;

    public List<Sprite> KillStreakSprite;
}