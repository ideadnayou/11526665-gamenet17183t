﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ClassSelection : MonoBehaviour
{
    public List<ShipData> ShipList;

    GameObject[] classList;

    [HideInInspector]
    public int index;

    Vector3 rotateTo;

    void Awake()
    {
        SceneManager.LoadScene("GameData", LoadSceneMode.Additive);
    }

    // Use this for initialization
    void Start()
    {
        classList = new GameObject[transform.childCount];

        for (int i = 0; i < transform.childCount; i++)
            classList[i] = transform.GetChild(i).gameObject;

        foreach (GameObject ship in classList)
            ship.SetActive(false);

        if (classList[index])
            classList[index].SetActive(true);

        rotateTo = Vector3.back;
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Rotate(rotateTo);
    }

    public void Toggle(bool change)
    {
        if (change)
        {
            classList[index].SetActive(false);
            index--;

            if (index < 0)
                index = ShipList.Count - 1;

            classList[index].SetActive(true);

        }
        else if (!change)
        {
            classList[index].SetActive(false);

            index++;

            if (index == ShipList.Count)
                index = 0;

            classList[index].SetActive(true);
        }
    }

    public void Confirm()
    {
        GameObject.FindObjectOfType<GameData>().mData = ShipList[index];

        SceneManager.LoadSceneAsync("Main", LoadSceneMode.Additive);
        SceneManager.UnloadScene("ClassSelection");
    }
}
