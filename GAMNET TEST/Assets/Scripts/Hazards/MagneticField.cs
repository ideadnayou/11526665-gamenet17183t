﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagneticField : Interactables
{    
    protected override void OnTriggerStay2D(Collider2D collision)
    {
        PlayerInput input = collision.GetComponent<PlayerInput>();
        if(input!=null)
            input.enabled = false;
    }
    protected override void OnTriggerExit2D(Collider2D collision)
    {
        PlayerInput input = collision.GetComponent<PlayerInput>();
        if (input != null)
            input.enabled = true;
    }
}
