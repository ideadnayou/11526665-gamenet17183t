﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : Interactables
{
    public float DamageOnCollision = 5f;
    public float MovementSpeed = 3f;
    public float DestroyAfter = 10f;
    private Vector2 movementDir = Vector2.right;
    public void SetDirection(Vector2 dir)
    {
        movementDir = dir;
    }
    protected override void Update()
    {
        Move();
        Destroy(gameObject, DestroyAfter);
    }
    void Move()
    {
        transform.Translate(movementDir * MovementSpeed * Time.deltaTime);
    }
    protected void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<PlayerData>())
        {
            PlayerTakeDamage(DamageOnCollision, collision);
            Destroy(gameObject);
        }
        else if (collision.gameObject.GetComponent<BaseBullet>())
        {
            Destroy(gameObject);
        }
    }

}
    