﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class AsteroidSpawner : NetworkBehaviour
{
    public Transform AboveScreen;
    public Transform DownScreen;
    public Transform LeftScreen;
    public Transform RightScreen;

    public int MinimumAsteroidPerGroup = 1;
    public int MaxAsteroidPerGroup = 3;
    public GameObject AsteroidPrefab;
    public float RadiusForSpawn = 10;
    public float SpawnIntervals = 5f;
    private float currentIntervalCount = 0;
    private Direction currentGroupDirection;
    private Vector2 currentGroupMovementDir;
    private Transform currentGroupTransformLocation;

    private Vector3 forDebug;
    public enum Direction
    {
        Up,
        Down,
        Left,
        Right
    }

    private void Update() { 
        UpdateSpawn();
    }
    void SetDirection(Direction direction)
    {
        switch (direction)
        {
            case Direction.Up:
                currentGroupMovementDir = Vector2.up;
                currentGroupTransformLocation = DownScreen;
                break;
            case Direction.Down:
                currentGroupMovementDir = Vector2.down;
                currentGroupTransformLocation = AboveScreen;
                break;
            case Direction.Left:
                currentGroupMovementDir = Vector2.left;
                currentGroupTransformLocation = RightScreen;
                break;
            case Direction.Right:
                currentGroupMovementDir = Vector2.right;
                currentGroupTransformLocation = LeftScreen;
                break;
        }
    }

    private Direction chooseDirection()
    {
        int choose = Random.Range(0, 3);
        if (choose == 0)
            return Direction.Up;
        else if (choose == 1)
            return Direction.Down;
        else if (choose == 2)
            return Direction.Left;
        else
            return Direction.Right;
    }

    void UpdateSpawn()
    {
        currentIntervalCount += Time.deltaTime;
        if (currentIntervalCount >= SpawnIntervals)
            SpawnAsteroidGroup();
    }

    void SpawnAsteroidGroup()
    {
        currentIntervalCount = 0;
        Direction chosenDir = chooseDirection();
        SetDirection(chosenDir);

        int spawnCount = Random.Range(MinimumAsteroidPerGroup, MaxAsteroidPerGroup);

        for (int x = 0; x <= spawnCount; x++)
        {
            Vector2 spawnLocationInsideCircle = Random.insideUnitCircle * RadiusForSpawn + new Vector2(currentGroupTransformLocation.position.x, currentGroupTransformLocation.position.y);
            float spawnX = spawnLocationInsideCircle.x + currentGroupTransformLocation.position.x;
            float spawnY = spawnLocationInsideCircle.y + currentGroupTransformLocation.position.y;
            //Vector3 realSpawnLocation = new Vector3(spawnX, spawnX, 0);
            Vector3 realSpawnLocation = new Vector3(spawnLocationInsideCircle.x, spawnLocationInsideCircle.y, 0);
            var asteroid = ((GameObject)Instantiate(AsteroidPrefab, realSpawnLocation, Quaternion.identity));
            asteroid.GetComponent<Asteroid>().SetDirection(currentGroupMovementDir);
            NetworkServer.Spawn(asteroid);
        }
    }
}

