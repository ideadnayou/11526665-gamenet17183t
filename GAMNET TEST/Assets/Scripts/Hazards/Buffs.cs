﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactables : MonoBehaviour
{
    public bool HasDuration;

    public float DurationOfBuff = 0f;

    private float currentDuration = 0f;

    protected virtual void Update()
    {
        if (HasDuration)
            TickDuration();
    }
    protected void TickDuration()
    {
        currentDuration += Time.deltaTime;
        if (currentDuration >= DurationOfBuff)
            Destroy(gameObject);
    }

    protected void PlayerTakeDamage(float damage, Collision2D coll)
    {
        if (coll.gameObject.GetComponent<PlayerData>())
        {
            coll.gameObject.GetComponent<PlayerData>().RpcTakeDamage(damage);
        }
    }
    protected void PlayerTakeDamageTrigger(float damage, Collider2D coll)
    {
        if (coll.gameObject.GetComponent<PlayerData>())
        {
            coll.gameObject.GetComponent<PlayerData>().RpcTakeDamage(damage);
        }
    }
    protected virtual void ActivateEffectTrigger(Collider2D coll) { }
    protected virtual void ActivateEffectCollision(Collision2D coll) { }
    protected virtual void OnTriggerStay2D(Collider2D collision) { }
    protected virtual void OnTriggerExit2D(Collider2D collision) { }
    protected virtual void OnTriggerEnter2D(Collider2D collision) { }
}
