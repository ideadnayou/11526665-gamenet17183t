﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class HazardSpawnerInsideLevel : NetworkBehaviour {
    public List<Interactables> HazardsList = new List<Interactables>();
    public  List<Interactables> currentHazards = new List<Interactables>();
    public float RadiusForSpawn = 20;
    public int NumberOfHazardsToUseSimultaneously = 1;
    public float SpawningIntervals = 5f;
    private float intervalCount = 0;
    private void Update()
    {
        int totalHazards = currentHazards.Count;
        intervalCount += Time.deltaTime;
        if (totalHazards < NumberOfHazardsToUseSimultaneously && intervalCount >= SpawningIntervals)
            SpawnHazard();

        DeleteNullsFromCurrentHazards();
    }
    void SpawnHazard()
    {
        intervalCount = 0;
        int totalHazards = HazardsList.Count;
        int spawnIndex = Random.Range(0, totalHazards);

        Vector2 spawnLocationRandom = Random.insideUnitCircle * RadiusForSpawn;
        Vector3 realSpawnLocation = new Vector3(spawnLocationRandom.x, spawnLocationRandom.y, 0);
       // var spawnRotation = Quaternion.Euler(0f, Random.Range(0, 100), 0);
        
       
        var hazard = ((GameObject)Instantiate(HazardsList[spawnIndex].gameObject, realSpawnLocation,Quaternion.identity));
        currentHazards.Add(hazard.GetComponent<Interactables>());
        NetworkServer.Spawn(hazard);
    }
    //call this function in update to see how big is the spawn points.
    void ForCheckingRadius()
    {
        Vector2 start = Random.insideUnitCircle * RadiusForSpawn;
        Debug.DrawRay(start, Vector3.forward, Color.green, 50.0f);
    }

    void DeleteNullsFromCurrentHazards()
    {
        currentHazards.RemoveAll(item => item == null);
    }
}
