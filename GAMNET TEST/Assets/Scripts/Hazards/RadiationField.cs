﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadiationField : Interactables
{
    public float DamageOverTime = 1f;

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerTakeDamageTrigger(DamageOverTime, collision);
    }

    protected override void OnTriggerStay2D(Collider2D collision)
    {
        PlayerTakeDamageTrigger(DamageOverTime, collision);
    }

}
