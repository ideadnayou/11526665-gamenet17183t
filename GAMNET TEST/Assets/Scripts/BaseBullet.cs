﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class BaseBullet : NetworkBehaviour
{
    public GameObject ImpactEffect;
    public float maxDamage;
    public float minDamage;
    public float BulletDamage;
    public float speed = 2;

    private Vector2 direction;

    protected virtual void Start()
    {
        BulletDamage = Random.Range(minDamage, maxDamage);
    }

    protected virtual void Update()
    {
        Destroy(this.gameObject, 2);
    }

    public void SetDirection(Vector2 dir)
    {
        direction = dir;
    }

    public virtual void Effect()
    {

    }

    protected void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<PlayerData>())
        {
            collision.gameObject.GetComponent<PlayerData>().RpcTakeDamage(BulletDamage);
            Destroy(gameObject);
        }
    }
}