﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClassSelectionUI : MonoBehaviour
{
    public Text ShipType;
    public Text ShipMinSpeed;
    public Text ShipMaxSpeed;
    public Text FireRate;
    public Text ShipHealth;
    public Text DamageReduction;

    ClassSelection classData;

    // Use this for initialization
    void Start()
    {
        classData = (ClassSelection)GameObject.FindObjectOfType<ClassSelection>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateShipStats();
    }

    void UpdateShipStats()
    {
        ShipType.text = ("Ship Class: " + classData.ShipList[classData.index].Type.ToString());
        ShipMaxSpeed.text = ("Ship Speed: " + classData.ShipList[classData.index].maxSpeed.ToString());
        ShipMinSpeed.text = ("Ship Speed: " + classData.ShipList[classData.index].minSpeed.ToString());
        ShipHealth.text = ("Ship Health: " + classData.ShipList[classData.index].MaxHP.ToString());
        FireRate.text = ("Fire Rate: " + classData.ShipList[classData.index].FireRate.ToString());
        DamageReduction.text = ("Damage Reduction: " + classData.ShipList[classData.index].DamageReduction.ToString());
    }
}